CONFIG = {
    "DJANGO_LOG_LEVEL": "INFO",
    "DATABASE": {
        "HOST": "platformtools-db",
        "USERNAME": "livspace",
        "PASSWORD": "livspaceadmin",
        "DB_NAME": "docket",
        "PORT": "3306"
    },
    "ENV": {
        "EVENT": "alpha2",
        "ENV": "alpha2",
        "ES": "alpha"
    },
    "DEBUG": False,
    "ALLOWED_HOSTS": ["*"],
    "X-Auth-Token": "8fb3d0d8-4bae-5e04-af14-5de7f049d5dd",
    "ELASTICSEARCH": {
        "PORT": 443,
        "TIMEOUT": 30,
        "MAX_RETRIES": 10,
        "RETRY_ON_TIMEOUT": True,
        "AWS_HOST_REGION": 'ap-southeast-1',
        "HOST": 'search-docket-dntir6nxy4mukfa7kdqjgfcrl4.ap-southeast-1.es.amazonaws.com',
        "AWS_ACCESS_KEY": 'AKIARPQNMXYNHWS3YY4I',
        "AWS_SECRET_KEY": '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'
    },
    "URL_CONF": {
        "cms": {
            "base_url": "api.alpha2.livspace.com/cms/",
            "headers": {"Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
                        "Content-Type": "application/json"},
            "gola_feature": {
                "id_key": "id_feature_value",
                "id_value": 17466,
                "feature_value_key": "value",
                "feature_value": "Yes"
            },
        },
        "cms_orch": {
            "base_url": "api.alpha2.livspace.com/cmsapi/",
            "headers": {"Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln", "Content-Type": "application/json"},
            "timeout": 200
        },
        "lego": {
            "base_url": "lego.alpha2.livspace.com/",
            "headers": {
                "X-CLIENT-ID": "LAUNCHPAD",
                "X-CLIENT-SECRET": "6ee38134-64c0-4095-af8a-38f05e69b0bd",
                "X-Requested-By": "20"
            }
        },
        "bouncer": {
            "base_url": "api.alpha2.livspace.com/bouncer/",
            "headers": {"Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
                        "Content-Type": "application/json"}
        },
        "zms": {
            "base_url": "zms.alpha2.livspace.com/",
            "headers": {
                "X-CLIENT-ID": "LAUNCHPAD",
                "X-CLIENT-SECRET": "6ee38134-64c0-4095-af8a-38f05e69b0bd"
            }
        }
    },
    
    "CELERY": {
        "BROKER_URL": 'redis://redis-lego:6379/3',
        "CELERY_TASK_SERIALIZER": "json",
        "CELERY_ACCEPT_CONTENT": ['application/json'],
        "CELERY_RESULT_SERIALIZER": 'json',
        "CELERY_CONTENT_ENCODING": 'utf-8',
        "CELERY_TIMEZONE": 'Asia/Calcutta',
    },
    "PRODUCT_MODULE_TYPE_FILTERS": {
        "modular": [{
            "attr": "super_category_id",
            "value": ["572bfa8f-cd00-48f5-9f9f-682e07b03a25", "ddfbd988-34ef-4464-b33e-d6b55fa4f5ef"],
            "type": "terms"
        }],
        "non_modular": [{
            "attr": "super_category_id",
            "value": "e3c17dac-572d-4d92-9ddb-f362d1248d07",
            "type": "term"
        }]
    },
    "use_seeker": False,
    "LEGO_MODULE_TYPE_MAPPING": {
        'default': "new-prop",
        152: 'electrical-prop'
    }
}
