'use strict';
const api_root = "http://axle";

const config = {
    axle : {
        "host": api_root,
        "headers": {
            "X-Client-Id": "Legion-Stage",
            "X-Client-Secret": "KKiOvHYqHkrVoNNjnREDLh0ssLPMS4sF",
            "X-Requested-By": ""
        }
    },
    vms : {
        "host" : api_root + "/proxy/vms",
        "headers" : {}
    },
    wms : {
        "host" : api_root + "/proxy/wms",
        "headers" : {}
    },
    cms : {
        "host": api_root + "/proxy/cms",
        "headers": {}
    },
    cmsapi: {
        "host": api_root + "/proxy/cmsapi",
        "headers": {}
    },
    customer: {
        "host": api_root + "/proxy/ps",
        "headers" : {}
    },
    address: {
        "host": api_root + "/proxy/ps",
        "headers": {}
    },
    ps: {
        "host": api_root + "/proxy/ps",
        "headers": {}
    },
    oms: {
        "host": api_root + "/proxy/oms",
        "headers" : {}
    },
    ims: {
        "host": api_root + "/proxy/ims",
        "headers" : {}
    },
    fms: {
        "host": api_root + "/proxy/fms",
        "headers" : {}
    },
    template: {
        "host": api_root + "/proxy/template",
        "headers" : {}
    },
    legion: {
        "host": api_root + "/proxy/legion",
        "headers" : {}
    },
    lms: {
        "host": api_root + "/proxy/lms",
        "headers" : {}
    },
    carbon: {
        "host": "http://carbon",
        "headers": {"X-Auth-Token": "VQmcwb8rlxRAr2KqpAXYo1XZ8ZCscDqh"}
    },
    bouncer: {
        "host": "http://bouncer.alpha2.livspace.com",
        "headers": {
            "X-CLIENT-ID": "LAUNCHPAD",
            "X-CLIENT-SECRET": "6ee38134-64c0-4095-af8a-38f05e69b0bd"
        }
    }
};
module.exports = config;
