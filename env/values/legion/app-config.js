'use strict';
module.exports = {
    environment: 'local',
    port : 3020,
    log_level : "debug",
    log_dir : "/var/log/legion-dev/",
    x_auth_token: "KKiOvHYqHkrVoNNjnREDLh0ssLPMS4sF",
    bouncer_app_id : 1636,
    key_value_store: "kv-store-dev",
    loggers: ["sysout"],
    feature_map: {
        NORMAL_PACKING_ID: 17344,
        CRATE_PACKING_ID: 17345,

        SOURCING_FEATURE_ID: 13,
        INVENTORY_FEATURE_VALUE_ID: 638,

        PACKING_CONF_FEATURE_ID: 289,
        PREDEFINED_CARCASS_PACKING_VALUE_ID : 17346,
        MODULE_WISE_PACKING_VALUE_ID : 17347,
        COMBINED_PO_WISE_PACKING_VALUE_ID : 17348,
        WEIGHT_WISE_SHUTTER_PACKING_VALUE_ID : 17349,
        PREDEFINED_SHUTTER_PACKING_VALUE_ID : 17350,

        PROCUREMENT_FEATURE_ID: 293,
        SHUTTER_PROCUREMENT_VALUE_ID: 17357,
        CARCASS_PROCUREMENT_VALUE_ID: 17358,

        INVENTORY_FEATURE_ID: 287,
        IH_INVENTORY_VALUE_ID: 17342,
        MH_INVENTORY_VALUE_ID: 17343,
        
        VENDOR_CAPACITY_REDUCTION_FEATURE_ID : 295,
        VENDOR_CAPACITY_REDUCTION_ID_VALUE : 17361,
        VENDOR_CAPACITY_NON_REDUCTION_ID_VALUE : 17362
    }
};
